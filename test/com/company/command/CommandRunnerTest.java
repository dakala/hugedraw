package com.company.command;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class CommandRunnerTest {
    private final String output = "----------------------\n|                    |\n|                    |\n|                    |\n|                    |\n----------------------\n\n----------------------\n|                    |\n|******              |\n|                    |\n|                    |\n----------------------\n\n----------------------\n|                    |\n|******              |\n|     *              |\n|     *              |\n----------------------\n\n----------------------\n|             *****  |\n|******       *   *  |\n|     *       *****  |\n|     *              |\n----------------------\n\n----------------------\n|ooooooooooooo*****oo|\n|******ooooooo*   *oo|\n|     *ooooooo*****oo|\n|     *oooooooooooooo|\n----------------------\n" + System.lineSeparator();
    private ByteArrayOutputStream outContent;
    private final PrintStream actualOutStream;

    @Before
    public final void before() {
        this.outContent = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(this.outContent);

        System.setOut(ps);
    }

    @After
    public final void after() {
        System.setOut(this.actualOutStream);
    }

    @Test
    public void testRunCommand() {
        CommandRunner commandRunner = new CommandRunner();

        commandRunner.runCommand("C 20 4");
        commandRunner.runCommand("L 1 2 6 2");
        commandRunner.runCommand("L 6 3 6 4");
        commandRunner.runCommand("R 14 1 18 3");
        commandRunner.runCommand("B 10 3 o");

        String separator = System.lineSeparator();
        String outputString = this.output.replaceAll(separator, "\n");
        String outContentString = this.outContent.toString().replaceAll(separator, "\n");

        Assert.assertEquals(outputString, outContentString);
    }

    public CommandRunnerTest() {
        this.actualOutStream = System.out;
    }
}
