package com.company.action;

import com.company.canvas.CanvasConstants;
import com.company.canvas.ICanvas;
import com.company.canvas.ICanvas.DefaultCanvas;
import com.company.canvas.Position;
import com.company.common.Failed;
import com.company.common.Status;
import com.company.common.Success;
import kotlin.collections.CollectionsKt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PaintLine implements IPainter {

    private final List affectedPositions;
    private final Position start;
    private final Position end;

    @Override
    public Status validate(ICanvas canvas) {
        Status paintStatus;
        if (!canvas.isPositionAvailable(this.start)) {
            paintStatus = (Status) (new Failed("Start position could not be found"));
        } else if (!canvas.isPositionAvailable(this.end)) {
            paintStatus = (Status) (new Failed("End position could not be found"));
        } else if ((this.start.getX() == this.end.getX()) == (this.end.getY() == this.start.getY())) {
            paintStatus = (Status) (new Failed("Only horizontal or vertical lines can be drawn"));
        } else {
            paintStatus = (Status) (new Success());
        }

        return paintStatus;
    }

    @Override
    public Status paint(ICanvas canvas) {
        Status validationStatus = this.validate(canvas);
        if (validationStatus instanceof Failed) {
            return validationStatus;
        } else {
            Iterable pixelValueBetween = (Iterable) DefaultCanvas.setPixelValueBetween(canvas, this.start, this.end, CanvasConstants.LINE_DISPLAY_CHAR, false, 8, (Object) null);
            CollectionsKt.addAll((Collection) this.affectedPositions, pixelValueBetween);

            return (Status) (new Success());
        }
    }

    public PaintLine(Position start, Position end) {
        super();
        this.start = start;
        this.end = end;
        this.affectedPositions = (List) (new ArrayList());
    }
}
