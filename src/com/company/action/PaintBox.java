package com.company.action;


import com.company.canvas.CanvasConstants;
import com.company.canvas.ICanvas;
import com.company.canvas.ICanvas.DefaultCanvas;
import com.company.canvas.Position;
import com.company.common.Failed;
import com.company.common.Status;
import com.company.common.Success;
import kotlin.collections.CollectionsKt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class PaintBox implements IPainter {

    private final LinkedHashSet affectedPosition;
    private final Position start;
    private final Position end;

    @Override
    public Status validate(ICanvas canvas) {
        Status paintStatus;
        if (!canvas.isPositionAvailable(this.start)) {
            paintStatus = (Status) (new Failed("Start position is not available"));
        } else if (!canvas.isPositionAvailable(this.end)) {
            paintStatus = (Status) (new Failed("End position is not available"));
        } else if (this.start.getX() == this.end.getX()) {
            paintStatus = (Status) (new Failed("Column positions are the same. Unable to draw box"));
        } else if (this.start.getY() == this.end.getY()) {
            paintStatus = (Status) (new Failed("Row positions are the same. Unable to draw box"));
        } else {
            paintStatus = (Status) (new Success());
        }

        return paintStatus;
    }

    @Override
    public Status paint(ICanvas canvas) {
        Status validationStatus = this.validate(canvas);
        if (validationStatus instanceof Failed) {
            return validationStatus;
        } else {
            List positions = (List) (new ArrayList());

            Collection positionsCollection = positions;
            Iterable iterablePixels = (Iterable) DefaultCanvas.setPixelValueBetween(canvas, this.start, new Position(this.start.getX(), this.end.getY()), CanvasConstants.LINE_DISPLAY_CHAR, false, 8, (Object) null);
            CollectionsKt.addAll(positionsCollection, iterablePixels);

            iterablePixels = (Iterable) DefaultCanvas.setPixelValueBetween(canvas, new Position(this.start.getX(), this.end.getY()), this.end, CanvasConstants.LINE_DISPLAY_CHAR, false, 8, (Object) null);
            CollectionsKt.addAll(positionsCollection, iterablePixels);

            iterablePixels = (Iterable) DefaultCanvas.setPixelValueBetween(canvas, this.end, new Position(this.end.getX(), this.start.getY()), CanvasConstants.LINE_DISPLAY_CHAR, false, 8, (Object) null);
            CollectionsKt.addAll(positionsCollection, iterablePixels);

            iterablePixels = (Iterable) DefaultCanvas.setPixelValueBetween(canvas, new Position(this.end.getX(), this.start.getY()), this.start, CanvasConstants.LINE_DISPLAY_CHAR, false, 8, (Object) null);
            CollectionsKt.addAll(positionsCollection, iterablePixels);

            this.affectedPosition.addAll(positions);

            return (Status) (new Success());
        }
    }

    public final LinkedHashSet getAffectedPosition() {

        return this.affectedPosition;
    }

    protected final Position getStart() {

        return this.start;
    }

    protected final Position getEnd() {

        return this.end;
    }

    public PaintBox(Position start, Position end) {
        super();
        this.start = start;
        this.end = end;
        this.affectedPosition = new LinkedHashSet();
    }

}
