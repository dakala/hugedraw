package com.company.canvas;

/**
 * Position.java
 *
 * A representation of the coordinates of points on the canvas.
 */
public class Position {
    private final int x;
    private final int y;

    public final int getX() {
        return this.x;
    }

    public final int getY() {
        return this.y;
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
}