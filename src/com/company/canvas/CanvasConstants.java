package com.company.canvas;

public final class CanvasConstants {
    public static final String WIDTH_BORDER_CHAR = "-";
    public static final String HEIGHT_BORDER_CHAR = "|";
    public static final String DEFAULT_DISPLAY_CHAR = " ";
    public static final String INVALID_TEXT_CHAR = "~";
    public static final String LINE_DISPLAY_CHAR = "*";
    public static final String EMPTY_STRING_CHAR = "";
}
