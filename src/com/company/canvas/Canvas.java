package com.company.canvas;

import kotlin.ranges.IntProgression;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt;

import java.util.ArrayList;
import java.util.List;

public class Canvas implements ICanvas {
    private Pixel[][] pixels;
    private int width;
    private int height;

    @Override
    public boolean createCanvas(int width, int height) {
        boolean created;
        if (this.width != 0 && this.height != 0) {
            created = false;
        } else {
            this.width = width;
            this.height = height;
            this.pixels = this.initializePixel(width, height);
            created = true;
        }

        return created;
    }

    @Override
    public void printScreen() {
        int i = 0;

        for (int xLength = this.pixels.length; i < xLength; ++i) {
            if (i != 0) {
                System.out.println(CanvasConstants.EMPTY_STRING_CHAR);
            }

            int j = 0;

            for (int yLength = this.pixels[i].length; j < yLength; ++j) {
                Pixel pixel = this.pixels[i][j];
                System.out.print(pixel);
            }
        }
        System.out.println();
    }

    @Override
    public boolean isPositionAvailable(Position pos) {
        Object[] paintedPixels = (Object[]) this.pixels;

        if (paintedPixels.length == 0) {
            return false;
        } else {
            return pos.getX() > 0 && pos.getY() > 0 && ((Object[]) this.pixels).length - 1 > pos.getY() && this.pixels[0].length - 1 > pos.getX();
        }
    }

    @Override
    public boolean isPositionWritable(Position pos) {
        return this.isPositionAvailable(pos) && this.pixels[pos.getY()][pos.getX()].isBlank();
    }

    @Override
    public String getPixelValueAt(Position pos) {
        return this.isPositionAvailable(pos) ? this.pixels[pos.getY()][pos.getX()].getText() : CanvasConstants.INVALID_TEXT_CHAR;
    }

    @Override
    public boolean setPixelValueAt(Position pos, String str, boolean overwrite) {
        boolean valueSet;
        if (!overwrite && this.isPositionWritable(pos)) {
            this.pixels[pos.getY()][pos.getX()].setText(str);
            valueSet = true;
        } else if (overwrite) {
            this.pixels[pos.getY()][pos.getX()].setText(str);
            valueSet = true;
        } else {
            valueSet = false;
        }

        return valueSet;
    }

    @Override
    public List setPixelValueBetween(Position inclusiveStart, Position inclusiveEnd, String str, boolean var4) {
        boolean var8 = false;

        List affectedList = (List) (new ArrayList());
        if (this.isPositionAvailable(inclusiveStart) && this.isPositionAvailable(inclusiveEnd)) {
            IntProgression progression;
            if (inclusiveStart.getY() <= inclusiveEnd.getY()) {
                progression = (IntProgression) (new IntRange(inclusiveStart.getY(), inclusiveEnd.getY()));
            } else {
                progression = RangesKt.downTo(inclusiveStart.getY(), inclusiveEnd.getY());
            }

            IntProgression yRange = progression;
            int y;
            if (inclusiveStart.getX() <= inclusiveEnd.getX()) {
                y = inclusiveStart.getX();
                progression = (IntProgression) (new IntRange(y, inclusiveEnd.getX()));
            } else {
                progression = RangesKt.downTo(inclusiveStart.getX(), inclusiveEnd.getX());
            }

            IntProgression xRange = progression;
            y = yRange.getFirst();
            int var11 = yRange.getLast();
            int var12 = yRange.getStep();
            if (var12 >= 0) {
                if (y > var11) {
                    return affectedList;
                }
            } else if (y < var11) {
                return affectedList;
            }

            while (true) {
                label42:
                {
                    int x = xRange.getFirst();
                    int var14 = xRange.getLast();
                    int var15 = xRange.getStep();
                    if (var15 >= 0) {
                        if (x > var14) {
                            break label42;
                        }
                    } else if (x < var14) {
                        break label42;
                    }

                    while (true) {
                        Pixel pixel = this.pixels[y][x];
                        pixel.setText(str);
                        affectedList.add(new Position(x, y));
                        if (x == var14) {
                            break;
                        }

                        x += var15;
                    }
                }

                if (y == var11) {
                    return affectedList;
                }

                y += var12;
            }
        } else {
            return affectedList;
        }
    }

    @Override
    public List writableChildrenOf(Position pos) {
        boolean var3 = false;
        List list = (List) (new ArrayList());
        if (this.isPositionWritable(new Position(pos.getX(), pos.getY() + 1))) {
            list.add(new Position(pos.getX(), pos.getY() + 1));
        }

        if (this.isPositionWritable(new Position(pos.getX(), pos.getY() - 1))) {
            list.add(new Position(pos.getX(), pos.getY() - 1));
        }

        if (this.isPositionWritable(new Position(pos.getX() + 1, pos.getY()))) {
            list.add(new Position(pos.getX() + 1, pos.getY()));
        }

        if (this.isPositionWritable(new Position(pos.getX() - 1, pos.getY()))) {
            list.add(new Position(pos.getX() - 1, pos.getY()));
        }

        return list;
    }


    private Pixel[][] initializePixel(int width, int height) {
        int var3 = height + 2;
        Pixel[][] var4 = new Pixel[var3][];

        for (int var5 = 0; var5 < var3; ++var5) {
            int i = var5;

            int var8 = width + 2;
            Pixel[] var9 = new Pixel[var8];

            for (int var10 = 0; var10 < var8; ++var10) {
                Pixel var15 = i != 0 && i != height + 1 ? (var10 != 0 && var10 != width + 1 ? new Pixel(CanvasConstants.DEFAULT_DISPLAY_CHAR) : (Pixel) (new HeightBorder())) : (Pixel) (new WidthBorder());
                var9[var10] = var15;
            }

            var4[var5] = var9;
        }

        return (Pixel[][]) var4;
    }

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        this.pixels = this.initializePixel(this.width, this.height);
    }

    public Canvas(int var1, int var2, int var3, int var4) {
        this(var1, var2);

        if ((var3 & 1) != 0) {
            var1 = 0;
        }

        if ((var3 & 2) != 0) {
            var2 = 0;
        }
    }

    public Canvas(int var1, int var2, int var3) {
        this(var1, var2);

        if ((var3 & 1) != 0) {
            var1 = 0;
        }

        if ((var3 & 2) != 0) {
            var2 = 0;
        }
    }


}
