package com.company.action;


import com.company.canvas.ICanvas;
import com.company.common.Status;
import com.company.common.Success;


public interface IPainter {
    Status validate(ICanvas canvas);

    Status paint(ICanvas canvas);

    public static final class DefaultPainter {
        public static Status validate(IPainter painter, ICanvas canvas) {

            return (Status) (new Success());
        }
    }
}
