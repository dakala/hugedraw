package com.company.action;

import com.company.canvas.ICanvas;
import com.company.canvas.Position;
import com.company.common.Failed;
import com.company.common.Status;
import com.company.common.Success;

public class PaintRectangle extends PaintBox {
    @Override
    public Status validate(ICanvas canvas) {
        Status paintStatus;

        Status status = super.validate(canvas);
        if (status instanceof Failed) {
            paintStatus = status;
        } else {
            int var3 = this.getStart().getX() - this.getEnd().getX();
            int var5 = Math.abs(var3);
            var3 = this.getStart().getY() - this.getEnd().getY();
            if (var5 == Math.abs(var3)) {
                paintStatus = (Status) (new Failed("Position forms square not rectangle"));
            } else {
                paintStatus = (Status) (new Success());
            }
        }

        return paintStatus;
    }

    public PaintRectangle(Position start, Position end) {
        super(start, end);
    }
}
