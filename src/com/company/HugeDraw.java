package com.company;

import com.company.command.CommandRunner;

/**
 * HugeDraw.java
 *
 */
public class HugeDraw {
    public static void main(String[] args) {
        CommandRunner commandRunner = new CommandRunner();
        commandRunner.start();
    }
}
