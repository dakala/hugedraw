package com.company.action;

import com.company.canvas.ICanvas;
import com.company.canvas.Position;
import com.company.common.Failed;
import com.company.common.Status;
import com.company.common.Success;
import kotlin.collections.CollectionsKt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PaintBucket implements IPainter {
    private final List affectedPositions;
    private final Position start;
    private final String colour;


    public final List getAffectedPositions() {
        return this.affectedPositions;
    }

    @Override
    public Status validate(ICanvas canvas) {
        return !canvas.isPositionAvailable(this.start) ? (Status) (new Failed("Position Not available")) : (!canvas.isPositionWritable(this.start) ? (Status) (new Failed("Position Filled already")) : (Status) (new Success()));
    }

    @Override
    public Status paint(ICanvas canvas) {
        Status paintStatus;

        Status status = this.validate(canvas);
        if (status instanceof Failed) {
            paintStatus = status;
        } else {
            paintStatus = (Status) this.bucketFill(canvas);
        }

        return paintStatus;
    }


    private final Success bucketFill(ICanvas canvas) {
        Position temp = this.start;

        List trackList = (List) (new ArrayList());
        canvas.setPixelValueAt(temp, this.colour, false);
        this.affectedPositions.add(temp);
        List children = canvas.writableChildrenOf(temp);
        Iterable iterableChildren = (Iterable) children;

        Iterator var7 = iterableChildren.iterator();

        Object element$iv;
        Position p;

        while (var7.hasNext()) {
            element$iv = var7.next();
            p = (Position) element$iv;

            canvas.setPixelValueAt(p, this.colour, false);
            this.affectedPositions.add(p);
            trackList.add(p);
        }

        while (trackList.size() > 0) {
            temp = (Position) CollectionsKt.last(trackList);
            trackList.remove(trackList.size() - 1);
            children = canvas.writableChildrenOf(temp);
            iterableChildren = (Iterable) children;

            var7 = iterableChildren.iterator();

            while (var7.hasNext()) {
                element$iv = var7.next();
                p = (Position) element$iv;

                canvas.setPixelValueAt(p, this.colour, false);
                this.affectedPositions.add(p);
                trackList.add(p);
            }
        }

        return new Success();
    }

    public PaintBucket(Position start, String colour) {
        super();
        this.start = start;
        this.colour = colour;
        boolean var3 = false;
        this.affectedPositions = (List) (new ArrayList());
    }
}
