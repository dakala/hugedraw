package com.company.canvas;

public class HeightBorder extends Pixel {

    public HeightBorder() {
        super(CanvasConstants.HEIGHT_BORDER_CHAR);
    }
}
