package com.company.canvas;

public class Pixel {
    private String text;

    public final String getText() {
        return this.text;
    }

    public final void setText(String str) {
        this.text = str;
    }

    public final boolean isBlank() {
        return this.text == null || this.text.trim().isEmpty();
    }

//    public int hashCode() {
//        return this.text.hashCode();
//    }

    public String toString() {
        return this.text;
    }

    public Pixel() {
        this.text = CanvasConstants.EMPTY_STRING_CHAR;
    }

    public Pixel(String str) {
        this();
        this.text = str;
    }
}
