package com.company.command;

import com.company.action.IPainter;
import com.company.canvas.Canvas;
import com.company.canvas.ICanvas;
import com.company.common.Status;

import java.util.Scanner;

public class CommandRunner {

    private final ICanvas canvas = (ICanvas) (new Canvas(0, 0));

    public void start() {
        this.runCommands();
    }

    public void runCommand(String command) {
        CommandParser commandParser = new CommandParser();
        IPainter painter = commandParser.parse(command);

        if (painter != null) {
            Status status = painter.paint(this.canvas);
            if (status.isSuccess()) {
                this.canvas.printScreen();
            }

            Status.StatusProcessor.process(status);
        }
    }

    public void runCommands() {
        String command = null;

        do {
            System.out.print("enter command: ");
            command = this.readNextLine();
            this.runCommand(command);

        } while (true);

    }

    private String readNextLine() {
        Scanner scan = new Scanner(System.in);
        return scan.nextLine();
    }

}
