package com.company.common;

public final class Success extends Status {

    public Success() {
        super(StatusTypes.SUCCESS);
    }
}
