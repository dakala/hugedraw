package com.company.common;

public final class Failed extends Status {

    private final String reason;

    public final String getReason() {
        return this.reason;
    }

    public Failed(String reason) {
        super(StatusTypes.FAILED);
        this.reason = reason;
    }
}