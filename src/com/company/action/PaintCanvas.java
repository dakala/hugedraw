package com.company.action;

import com.company.canvas.ICanvas;
import com.company.common.Failed;
import com.company.common.Status;
import com.company.common.Success;

public class PaintCanvas implements IPainter {

    private final int width;
    private final int height;

    public PaintCanvas(int width, int height) {
        this.width = width;
        this.height = height;
    }


    @Override
    public Status validate(ICanvas canvas) {
        return DefaultPainter.validate(this, canvas);
    }

    @Override
    public Status paint(ICanvas canvas) {
        Status paintStatus;
        if (this.width != 0 && this.height != 0) {
            boolean success = canvas.createCanvas(this.width, this.height);
            paintStatus = success ? (Status) (new Success()) : (Status) (new Failed("Canvas not created"));
        } else {
            paintStatus = (Status) (new Failed("Unable to create canvas - width: " + this.width + ", height: " + this.height));
        }

        return paintStatus;
    }
}
