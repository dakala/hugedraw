package com.company.command;

import com.company.action.*;
import com.company.canvas.Position;

public class CommandParser {

    public IPainter parse(String command) {
        IPainter painter = null;
        // Remove leading and trailing spaces before splitting entered string.
        String[] parts = command.trim().split("\\s+");

        // Remove trailing and leading spaces in each array element.
        for (int i = 0; i < parts.length; i++) {
            parts[i] = parts[i].trim();
        }

        // Get the first letter and convert to upper case.
        String firstCharacter = parts[0].toUpperCase();

        if (!(firstCharacter.length() == 1)) {
            painter = this.help();
        } else {
            switch (firstCharacter) {
                case "C":
                    painter = this.createPaintCanvas(parts);
                    break;

                case "L":
                    painter = this.createPaintLine(parts);
                    break;

                case "R":
                    painter = this.createPaintRectangle(parts);
                    break;

                case "B":
                    painter = this.createPaintBucket(parts);
                    break;

                case "Q":
                    this.quitCommand(command);
                    break;

                default:
                    painter = this.help();
            }
        }

        return painter;
    }


    public final IPainter createPaintCanvas(String[] parts) {
        if (parts.length != 3) {
            return null;
        }
        int width = Integer.parseInt(parts[1]);
        int height = Integer.parseInt(parts[2]);

        if (width > 0 && height > 0) {
            return (IPainter) (new PaintCanvas(width, height));
        } else {
            return null;
        }
    }

    public final IPainter createPaintLine(String[] parts) {
        if (parts.length == 5) {
            int x1 = Integer.parseInt(parts[1]);
            int y1 = Integer.parseInt(parts[2]);
            int x2 = Integer.parseInt(parts[3]);
            int y2 = Integer.parseInt(parts[4]);

            return (IPainter) (new PaintLine(new Position(x1, y1), new Position(x2, y2)));

        } else {
            return null;
        }
    }

    public final IPainter createPaintRectangle(String[] parts) {
        if (parts.length == 5) {
            int x1 = Integer.parseInt(parts[1]);
            int y1 = Integer.parseInt(parts[2]);
            int x2 = Integer.parseInt(parts[3]);
            int y2 = Integer.parseInt(parts[4]);

            return (IPainter) (new PaintRectangle(new Position(x1, y1), new Position(x2, y2)));

        } else {
            return null;
        }
    }

    public final IPainter createPaintBucket(String[] parts) {
        if (parts.length == 4) {
            int x1 = Integer.parseInt(parts[1]);
            int y1 = Integer.parseInt(parts[2]);
            String colour = parts[3];

            return (IPainter) (new PaintBucket(new Position(x1, y1), colour));

        } else {
            return null;
        }
    }

    private void quitCommand(String cmd) {
        System.out.println("Bye!");
        System.exit(0);
    }

    /**
     * Display help information about the program.
     *
     * @return IPainter painter
     */
    private IPainter help() {
        IPainter painter = null;

        System.out.println("Enter a space-delimited command with C, L, R, B or quit with Q. This is case-insensitive.");
        System.out.println("C w h - creates a canvas of width \"w\" and height \"h\" e.g. C 20 4");
        System.out.println("L x1 y1 x2 y2 - creates a line from (\"x1\", \"y1\") to (\"x2\", \"y2\"). This can only draw horizontal and vertical lines with \"x\" characters e.g. L 1 2 6 2");
        System.out.println("R x1 y1 x2 y2 - creates a rectangle, upper left corner (\"x1\", \"y1\") and lower right corner (\"x2\", \"y2\"). Lines are drawn with \"x\" characters e.g. R 16 1 20 3");
        System.out.println("B x y c - fills the area connected to (\"x\", \"y\") with \"c\" characters e.g. B 10 3");
        System.out.println("Q - quits the program");

        return painter;
    }
}
