package com.company.common;

import com.company.canvas.CanvasConstants;

public class Status {

    private final StatusTypes status;
    public static final Status.StatusProcessor StatusProcessor = new Status.StatusProcessor();

    public final boolean isSuccess() {
        return this.status == StatusTypes.SUCCESS;
    }

    public final StatusTypes getStatus() {
        return this.status;
    }

    public Status(StatusTypes status) {
        super();
        this.status = status;
    }


    public static final class StatusProcessor {
        public final void process(Status status) {
            String message = null;
            if (status instanceof Success) {
                message = CanvasConstants.EMPTY_STRING_CHAR;
            } else if (status instanceof Failed) {
                message = "ERROR: " + ((Failed) status).getReason();
            }
            System.out.println(message);
        }

        public StatusProcessor() { }

    }

}
