package com.company.canvas;

import java.util.List;

public interface ICanvas {

    boolean createCanvas(int width, int height);

    void printScreen();

    boolean isPositionAvailable(Position pos);

    boolean isPositionWritable(Position pos);

    String getPixelValueAt(Position pos);

    boolean setPixelValueAt(Position start, String end, boolean var3);

    List setPixelValueBetween(Position start, Position end, String character, boolean var4);

    List writableChildrenOf(Position pos);


    public static final class DefaultCanvas {

        public static boolean setPixelValueAt(ICanvas canvas, Position start, String end, boolean var3, int var4, Object obj) {
            if (obj != null) {
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setPixelValueAt");
            } else {
                if ((var4 & 4) != 0) {
                    var3 = true;
                }

                return canvas.setPixelValueAt(start, end, var3);
            }
        }

        public static List setPixelValueBetween(ICanvas canvas, Position start, Position end, String character, boolean y2, int var5, Object obj) {
            if (obj != null) {
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setPixelValueBetween");
            } else {
                if ((var5 & 8) != 0) {
                    y2 = true;
                }

                return canvas.setPixelValueBetween(start, end, character, y2);
            }
        }
    }
}
